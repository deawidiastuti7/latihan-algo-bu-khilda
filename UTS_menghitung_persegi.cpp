#include <stdio.h>

int main() {
  
  float sisi, luas, keliling;

  printf("Masukkan panjang sisi: ");
  scanf("%f", &sisi);

  //luas
  luas = sisi * sisi;

  //Keliling 
  keliling = 4 * sisi;

  //Hasil
  printf("Luas persegi: %.2f\n", luas);
  printf("Keliling persegi: %.2f\n", keliling);
  
  return 0;
}
