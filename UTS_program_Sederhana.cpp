#include <stdio.h>

int main() {

  char Nama[100];
  char NIM[10];
  char Kelas[20];

  printf("Masukkan Nama: ");
  scanf("%s", &Nama);

  printf("Masukkan NIM: ");
  scanf("%s", &NIM);

  printf("Masukkan Kelas: ");
  scanf("%s", &Kelas);

  printf("\nBiodata Siswa\n");
  printf("--------------\n");
  printf("Nama: %s\n", Nama);
  printf("NIM: %s\n", NIM);
  printf("Kelas: %s\n", Kelas);

  return 0;
}
