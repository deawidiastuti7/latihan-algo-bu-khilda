#include <stdio.h>

int main() {

  int i, j;

  for(i=1; i<=10; i++) {
   
    for(j=1; j<=i; j++) {
      printf("%d ", j);
      
      if(j < i) {
        printf("       "); //spasi
      }
    }
    
    printf("\n");
  }

  return 0;  
}
