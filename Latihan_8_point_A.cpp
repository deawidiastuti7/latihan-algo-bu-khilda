#include <stdio.h>

int main() {

  int matA[2][2], matB[2][2], matHasil[2][2];

  printf("Data Matriks A:\n");
  for(int i=0; i<2; i++) {
    for(int j=0; j<2; j++) {
      printf("Data Matriks A [%d][%d] : ", i, j);
      scanf("%d", &matA[i][j]); 
    }
  }

  printf("\nData Matriks B:\n");
  for(int i=0; i<2; i++) {
    for(int j=0; j<2; j++) {
      printf("Data Matriks B [%d][%d] : ", i, j);
      scanf("%d", &matB[i][j]);
    }
  }

  for(int i=0; i<2; i++) {
    for(int j=0; j<2; j++) {
      matHasil[i][j] = matA[i][j] + matB[i][j]; 
    }
  }


  printf("\nMatriks A + Matriks B:\n");
  for(int i=0; i<2; i++) {
    for(int j=0; j<2; j++) {
      printf("%d\t", matHasil[i][j]);
    }
    printf("\n");
  }

  return 0;
}
