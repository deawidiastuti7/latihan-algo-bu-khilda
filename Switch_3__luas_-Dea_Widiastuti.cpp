#include <stdio.h>

int main(void) {
int pilihan;
char ulang;
do{
printf("## Menghitung Luas Bangun Datar ## \n");
printf("================= == \n");
printf("1. Luas Persegi \n");
printf("2. Luas Segitiga \n");
printf("3. Luas Lingkaran \n\n");

printf("Pilihan anda: ");
scanf("%i",&pilihan);
switch(pilihan)
 {
 	case 1:
		printf("Menghitung Luas Persegi \n");
			int panjang,lebar,hasil;
			printf("Masukan panjang: ");
    		scanf("%d", &panjang);
    		printf("Masukan lebar: ");
    		scanf("%d", &lebar);
    		
    		hasil = panjang*lebar;
    		printf ("hasil : %d",hasil);
	break;
	case 2:
		printf("Menghitung Luas Segitiga \n");\
  			int alas,tinggi;
  			printf("Masukan alas: ");
  			scanf("%d", &alas);
  			printf("Masukan tinggi: ");
  			scanf("%d", &tinggi);

  			hasil = 0.5*alas*tinggi;
  			printf("hasil : %d", hasil);
	break;
	case 3:
		printf("Menghitung Luas Lingkaran \n");
			int jari_jari;
			float hasilnya;
  			printf("Masukan jari_jari: ");
  			scanf("%d", &jari_jari);

  			hasilnya = 3.14*jari_jari*jari_jari;
  			printf("hasil : %.2f", hasilnya);
	break;
	
	default:
		printf ("Pilihan Tidak Tersedia \n");
}
printf ("\n");
printf ("ingin menghitung luas bangun datar yang lain (y/t)? ");
scanf (" %c", &ulang);
printf ("\n");
}
while (ulang!= 't');
printf ("Selesai Alhamdulillah :) \n");
return 0;
}
