#include <stdio.h>

int main() {
  
  int baris, kolom;
  printf("Masukkan jumlah baris: ");
  scanf("%d", &baris);
  printf("Masukkan jumlah kolom: "); 
  scanf("%d", &kolom);
  
  int matriks[baris][kolom];
  
  // Input data matriks
  for(int i=0; i<baris; i++) {
    for(int j=0; j<kolom; j++) {
      printf("masukkan elemen matriks [%d][%d]: ", i, j);
      scanf("%d", &matriks[i][j]);
    }
  }
  
  // Tampilkan matriks
  printf("\nMatriks:\n");
  for(int i=0; i<baris; i++) {
    for(int j=0; j<kolom; j++) {
      printf("%d ", matriks[i][j]); 
    }
    printf("\n");
  }

  return 0;
}
