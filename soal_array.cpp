#include <stdio.h>

int main() {

  int bil[100], max, i, n;

  printf("Masukkan banyaknya bilangan: ");
  scanf("%d", &n);

  for(i=0; i<n; i++) {
    printf("Bilangan ke-%d : ", i+1);
    scanf("%d", &bil[i]);
  }

  max = bil[0];

  for(i=1; i<n; i++) {
    if(bil[i] > max) {
      max = bil[i]; 
    }
  }

  printf("Bilangan terbesar adalah %d", max);
  
  return 0;
}
