#include <stdio.h>

int main() {
  
  int banyak, i;
  float nilai, total=0;
  
  printf("Input banyak nilai: ");
  scanf("%d", &banyak);
  
  for(i=1; i<=banyak; i++) {
    printf("Input nilai %d: ", i);
    scanf("%f", &nilai);
    total += nilai;
  }
  
  float rata2 = total / banyak;
  
  printf("\nTotal nilai = %f\n", total); 
  printf("Rata-rata nilai = %f", rata2);
  
  return 0;
}

